# Clicker SDK
This is made to be used for Electron app on ubuntu.

## Step By Step Bash Commands

```bash
$ git clone git@bitbucket.org:taghive/desktop-clicker-sdk.git
$ git clone git@bitbucket.org:taghive/classkeydeskstopsdktestapp.git

$ cd desktop-clicker-sdk/
$ npm rebuild

$ cd ../classkeydeskstopsdktestapp
$ npm rebuild
$ npm install
$ npx electron-build
$ npm start
```

## Setup

``git clone https://taghive-chuck@bitbucket.org/taghive/desktop-clicker-sdk.git``

(preferablly in the same directory of the app)

go to app dir
npm install ../desktop-clicker-sdk

(Optional)
if you have a similar error as below

```bash
NODE_MODULE_VERSION 57. This version of Node.js requires
NODE_MODULE_VERSION 64. Please try re-compiling or re-installing
```

Please do following

```bash
npm install -D electron-build && npx electron-build
```

## Implement SDK init

### Go to main file for Electron

please check main.js in this project

### Import SDK module

```js
const clickerSDK = require('clickersdk');
```

## Change app ready callback codes

```js
app.on('ready', createWindow)
```

will change to (please refer to main.js:60)

```js
const appDir = '/home/taghive/Programming/Code/classkeydeskstopsdktestapp'
const appCommand = appDir+'/node_modules/.bin/electron ' + appDir + '/.'
clickerSDK.init(appCommand, () => {
  createWindow();
});
```

## Implement Register Clicker Codes

In ui codes that will handle registering clickers (This is necessary for enable clickers to handle multiple clicks at the same time)

Each clicker should have unique combination of classNum & studentNum!!

clickerNum below is the clicker number for usb dongle to process registration. If you pass 1, it will register the clicker to the classNum and studentNum only when a clicker presses 1.

```js
var classNum = 1;
var studentNum = 1;
var clickerNum = Math.floor(Math.random() * 4) + 2;
clickerSDK.registerStudent(classNum, studentNum, clickerNum)
  .then((clickerId) => {
    console.log('registered clickerId: ' + clickerId);
  });
clickerSDK.listenClickerEvent((eventNum, deviceID) => {
  console.log('listenClickerEvent callback is called');
  console.log('eventNum: ' + eventNum);
  console.log('deviceId: ' + deviceID);
});
```

If you cancel registering in the middle(close app or go to another page) please call the following

```js
clickerSDK.stopListening();
```

## Implement Listening Clicker Events

```js
clickerSDK.listenClickerEvent((eventNum, deviceID) => {
  console.log('listenClickerEvent callback is called');
  console.log('eventNum: ' + eventNum);
  console.log('deviceId: ' + deviceID);
});
```

If you need to stop listening events in the middle(close app or go to another page) please call the following(as you did with registering)

```js
clickerSDK.stopListening();
```
