const clickerSDK = require('clickersdk');

let count = 1;
let modal = document.getElementById('myModal');

// Code for testing listenClickerEvent;
clickerSDK.listenClickerEvent((eventNum, deviceID) => {
    $('.tbody').prepend('<tr><th scope="row">' + (count++) + '</th><td>' + deviceID + '</td><td>' + eventNum + '</td</tr>');
});

// Code for testing register
document.getElementById('register').onclick = function() {
    clickerSDK.stopListening();
    modal.style.display = 'block';
    document.getElementById('h1').innerHTML = '';
  
    window.onclick = function(event) {
        if (event.target == modal) {
            if (document.getElementById('registerKey') !== 'Register is successfull !') {
                clickerSDK.stopListening();
            }
            modal.style.display = 'none';
        }
    }

    var classNum = 1;
    var studentNum = 1;
    var clickerNum = Math.floor(Math.random() * 4) + 2;
    document.getElementById('startListening').innerHTML = 'Start Litening!';
    document.getElementById('registerKey').innerHTML = 'Press clicker number ' + clickerNum;
    clickerSDK.registerStudent(classNum, studentNum, clickerNum)
        .then((clickerId) => {
            document.getElementById('registerKey').innerHTML = 'Register is successfull !';
        });
}

document.getElementById('startListening').onclick = function() {
    if (document.getElementById('startListening') === 'Stop Listening!') {
        clickerSDK.stopListening();
    } else {
        document.getElementById('h1').innerHTML = 'Listening Clicker!';
        document.getElementById('startListening').innerHTML = 'Stop Listening!';
        clickerSDK.listenClickerEvent((eventNum, deviceID) => {
            $('.tbody').prepend('<tr><th scope="row">' + (count++) + '</th><td>' + deviceID + '</td><td>' + eventNum + '</td</tr>');
        });
    }
}
